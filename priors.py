# Definition of the marginal prior distributions in terms of their PDFs as
# specified by *********.

import numpy as np
from scipy.stats import norm


def log_uniform_PDF(x, mean, halfwidth):
    """Logarthmised PDF of a uniform distribution.

    For a uniform distribution on the interval
    (`mean` - `halfwidth`, `mean` + `halfwidth`) this function returns
    the value of the probability density function at `x`.
    """
    # The PDF of a uniform distribution on an interval is the indicator
    # function over the interval divided by the interval length. Here, the
    # inverval length is 2*halfwidth. Taking logarithms results in:
    scale_factor = -np.log(2) - np.log(halfwidth)

    # Evaluating the indicator function:
    indicator = (x < mean + halfwidth) and (x > mean - halfwidth)

    # Return the logarithm of the uniform PDF avoiding "np.log(0)":
    return np.where(indicator, scale_factor, -np.inf)


class PriorClass:
    """Generic prior distribution class meant for subclassing."""

    def logpdf(self, x):
        """Log of the prior probability density function at x."""
        pass

    def pdf(self, x):
        """Probability densitiy function at x."""
        return np.exp(self.logpdf(x))

    def L(self):
        """Left/lower endpoint of the x-axis when plotting the
        probability density function.
        """
        pass

    def R(self):
        """Right/upper endpoint of the x-axis when plotting the
        probability density function.
        """
        pass


class UniformDistribution(PriorClass):
    """Uniform probability distribution.

    Parameters
    ----------
    m : float
        Mean of the distribution.
    hw : positive float
        Half of the length of the interval on which the distribution is
        defined.
    """

    def __init__(self, m=0.5, hw=0.5):
        self.m = m
        self.hw = hw

    def logpdf(self, x):
        return log_uniform_PDF(x, self.m, self.hw)

    def L(self):
        return self.m - 1.3 * self.hw

    def R(self):
        return self.m + 1.3 * self.hw


class NormalDistribution(PriorClass):
    """Normal probability distribution.

    Parameters
    ----------
    m : float
        Mean of the distribution.
    sd : positive float
        Standard deviation of the distribution.
    """

    def __init__(self, m=0, sd=1):
        self.m = m
        self.sd = sd

    def logpdf(self, x):
        return norm.logpdf(x, self.m, self.sd)

    def L(self):
        return self.m - 4 * self.sd

    def R(self):
        return self.m + 4 * self.sd
