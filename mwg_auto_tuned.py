"""
Implementation of the auto-tuned Metropolis-within-Gibbs (MwG) algorithm.

Written by Jens B., 2015--2020
"""

from sys import stdout  # to print status update to terminal

import numpy as np


def proposal_sampler(current_value, tune):
    """
    Draw a random sample from a univariate Gaussian distribution.

    Inputs:
    ----------
    current_value  :  scalar
        The mean value.
    tune  :  scalar
        The standard deviation.

    """
    return np.random.normal(current_value, tune)


def my_MwG(theta0, N, unscaled_log_posterior, prop_tune, ku, burn_in):
    """
    Auto-tuned Metropolis-within-Gibbs sampler.

    Generates a Marokv chain of samples to approximate the posterior
    distribution of the parameters in a Bayesian statistical model.

    Inputs:
    ----------
    theta0  :  numpy array of length `d`
        The starting point of the Markov chain.
    N  :  int
        The length of the Markov chain, i.e. how many samples we wish to
        generate.
    unscaled_log_posterior  :  function
        Posterior PDF of the Bayesian model, unscaled and logarithmised. That
        is: the sum of log-likelihood and logarithmised prior PDF.
    prop_tune  :  numpy array of length `d`
        Start value for the standard deviations for the proposal distributions.
    ku  :  int
        Parameter to tune the algorithm. During the burn in phase the proposal
        distributions are updated every ku iterations.
    burn_in  :  int
        Number of iterations to be used for the burn in phase during which
        the proposal distributions are auto-tuned.

    Outputs:
    ----------
    theta_samples  :  [N+1, d]-dimensional numpy array
        N+1 samples of the d-dimensional posterior distribution, where
        theta_samples[0, ] is the starting point theta0.

    """
    # Rename for a more concise notation:
    ulp = unscaled_log_posterior

    d = len(theta0)

    # Initialise output array and record the zeroth sample theta0:
    theta_samples = np.zeros([N + 1, d])
    theta_samples[0, :] = theta0.copy()

    # Initialise array to store the last ku acceptance ratios:
    ar = np.zeros([ku, d])

    # The MwG algorithm:
    for k in range(N):
        theta_samples[k + 1, :] = theta_samples[k, :].copy()

        for j in range(d):
            # Propose a value for theta_samples[k+1, j]:
            proposal = proposal_sampler(theta_samples[k, j], prop_tune[j])

            # Construct a temporary version of theta_samples[k, ] with
            # theta_samples[k, 0:(j-1)] already updated,
            # theta_samples[k, j] equal to the proposal, and
            # theta_samples[k, (j+1):] from the previous sample:
            temp_theta = theta_samples[k + 1, :].copy()
            temp_theta[j] = proposal

            # Calculate the acceptance ratio:
            ar[(k + 1) % ku, j] = ulp(temp_theta) - ulp(theta_samples[k + 1, :])

            # Accept proposed sample with probability ar[(k+1)%ku, j]:
            u = np.random.uniform()
            if np.log(u) < ar[(k + 1) % ku, j]:
                theta_samples[k + 1, j] = 1 * proposal
            # No "else" needed due to the intialisation of
            # theta_samples[k+1, ] = theta_samples[k, ] before the j-loop.

        # Auto-tuning during burn in phase:

        # Every ku samples we consider all d proposal_tuning_parameters
        # and adjust them in order to hopefully obtain average acceptance
        # ratios between 20 and 40 percent:
        if (k + 1) % ku == 0 and (k + 1) < burn_in:
            for j in range(d):
                if np.average(ar[:, j]) > np.log(0.4):
                    prop_tune[j] = 1.001 * prop_tune[j]
                elif np.average(ar[:, j]) < np.log(0.2):
                    prop_tune[j] = 0.999 * prop_tune[j]

        # Print status update to terminal:  #TODO: Create status bar.
        if (status := k / N * 100) * 100 % 1 == 0:
            stdout.write(
                "\r%.2f %% of all %d samples have been generated." % (status, N)
            )

    return theta_samples
