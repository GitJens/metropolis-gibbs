#!/usr/bin/env python
"""
Example showing how to use the module mwg_auto_tuned.

"""

import numpy as np
from scipy.special import kv as imported_Bessel_function
from priors import UniformDistribution, NormalDistribution
import mwg_auto_tuned


#
# Define the model, step 1: Define likelihood.
#

def mean_function(f, theta):
    """
    Implementation of the function "g(.)" that specifies the mean in the
    ComplexGaussian likelihood as a function of the frequency f and the
    parameter theta.
    Based on eqs. (1,2,3) in Peleties et al. (2011).

    Inputs:
    ----------
    f  :  scalar
        The frequency of the current passed through the VW viscometer's wire.
    theta  :  numpy array of length 11
        The parameter vector.

    """
    Omega = 2 * np.pi * f * theta[0] * theta[3]**2 / theta[9]
    jj0 = (1j * Omega)**.5
    K0 = imported_Bessel_function(0, jj0)
    K1 = imported_Bessel_function(1, jj0)
    jjbetas = theta[0] / theta[2] * (1j + 4 * 1j * K1 / (jj0 * K0))
    betaprime = jjbetas.real
    beta = jjbetas.imag

    V = (theta[4] * 1j * f /
         (theta[5]**2 - f**2 * (1 + beta) +
         1j * f**2 * (betaprime + 2 * theta[1]))
         + theta[6] + theta[7] * 1j + theta[8] * 1j * f)

    return V


def likelihood(theta, fvals, Xvals, Yvals):
    """
    Evaluate the logarithmised likelihood (up to constants w.r.t. theta) given
    observations V = X + iY (= X + 1j * Y).

    Inputs:
    ----------
    theta  :  numpy array of length 11
        The parameter vector.
    fvals  :  numpy array
        Array of frequencies used to obtain the data.
    Xvals  :  numpy array of the same length as fvals
        Array of real parts of the observations V.
    Yvals  :  numpy array of the same length as fvals
        Array of imaginary parts of the observations V.

    """
    mean_values = [mean_function(f, theta) for f in fvals]

    return - sum(abs(Xvals + 1j * Yvals - mean_values)**2 / theta[10])


#
# Define the model, step 2: Define the prior distribution for all parameters.
#

# Set hyperparameters (means and interval halfwidths or standard deviations),
# define marginal prior distributions:

# rho:
_rho_m = 9.58060000e+02
prior_rho = UniformDistribution(_rho_m, .001 * _rho_m)

# Delta_0:
_Delta_0_m = 1.40600000e-04
prior_Delta_0 = NormalDistribution(_Delta_0_m, .1 * _Delta_0_m)

# rho_s:
_rho_s_m = 1.93000000e+04
prior_rho_s = UniformDistribution(_rho_s_m, .001 * _rho_s_m)

# R:
_R_m = 0.00015015
prior_R = NormalDistribution(_R_m, .5 * 1.2e-06)

# Lambda:
_Lambda_m = 8.32638824e-03
prior_Lambda = UniformDistribution(_Lambda_m, .1 * _Lambda_m)

# f_0:
_f_0_m = 1.33086617e+03
prior_f_0 = UniformDistribution(_f_0_m, 100.)

# a:
_a_m = 1.92221706e-07
_max_X_val = 6.77709e-05  # Largest value of X in the data.
_a_hw = .01 * _max_X_val
prior_a = UniformDistribution(_a_m, _a_hw)

# b:
_b_m = -4.73710464e-05
_max_Y_val = 0.000030935  # Largest value of Y in the data.
_b_hw = .01 * _max_Y_val
prior_b = UniformDistribution(_b_m, _b_hw)

# c:
_c_m = 3.65806801e-08
_f_max = 1204.7  # value of f that corresponds to max_Y_val aka. argmax_f Y(f)
_c_hw = .01 * _max_Y_val / _f_max
prior_c = UniformDistribution(_c_m, _c_hw)

# eta:
_eta_m = 4.61528369e-02
prior_eta = UniformDistribution(_eta_m, 0.1 * _eta_m)

# nu:
_nu_m = 2.67110084555751E-008
prior_nu = UniformDistribution(_nu_m, 0.1 * _nu_m)

# Join marginal priors into a joint prior:

marginal_priors = [prior_rho, prior_Delta_0, prior_rho_s, prior_R,
                   prior_Lambda, prior_f_0, prior_a, prior_b, prior_c,
                   prior_eta, prior_nu]


def prior(theta):
    """
    Evaluate the logarithmised joint prior PDF at theta.

    Input:
    ----------
    theta  :  numpy array of length 11
        The parameter vector.

    """
    mps = marginal_priors

    mps_evaluated = np.array([p.logpdf(t) for p, t in zip(mps, theta)])

    # Return value of the logarithmised joint prior PDF:
    return mps_evaluated.sum()


#
# Define the unscaled logarithmised posterior PDF:
# (Preliminarily also with data (f, X, Y) as an argument.
# Before passing this function to the MwG sampler we will re-define
# posterior(theta) as a function of the parameter theta only.)
#


def _posterior(theta, f, X, Y):
    """
    Evaluate the logarithmised posterior PDF (up to constants
    w.r.t. theta) given observations V = X + iY (= X + 1j * Y).

    Inputs:
    ----------
    theta  :  numpy array of length 11
        The parameter vector.
    fvals  :  numpy array
        Array of frequencies used to obtain the data.
    Xvals  :  numpy array of the same length as fvals
        Array of real parts of the observations V.
    Yvals  :  numpy array of the same length as fvals
        Array of imaginary parts of the observations V.

    """
    return likelihood(theta, f, X, Y) + prior(theta)


#
# Import data, set up and run MwG-algorithm, save results:
#

# Import measured values of frequency f as well as X and Y:
fvals, Xvals, Yvals = np.genfromtxt('data.csv', delimiter=',',
                                    usecols=(0, 1, 2)).transpose()


def posterior(theta):
    """
    Logarithmised unscaled posterior PDF as function of only the parameter
    theta, using the previously imported data.

    """
    return _posterior(theta, fvals, Xvals, Yvals)


# Use prior means as starting point for the Markov chain of samples:
theta_start = np.array([_rho_m, _Delta_0_m, _rho_s_m, _R_m, _Lambda_m, _f_0_m,
                        _a_m, _b_m, _c_m, _eta_m, _nu_m])

# Generate `N` samples, out of which `burn_in` are used as burn-in-phase:
N = 250_000
burn_in = 50_000

# During the burn in period the proposal distribution is updated every `ku`
# steps:
ku = 5

# Start value for the standard deviation of the proposal distribution:
tune = np.array([0.08, 2e-05, 2.2, 9e-07,
                 7e-05, 21, 1e-07, 3e-08,
                 3e-11, 3e-04, 3e-10])

# Run algorithm to obtain samples:
my_samples = mwg_auto_tuned.my_MwG(
                theta0=theta_start, N=N,
                unscaled_log_posterior=posterior,
                prop_tune=tune, ku=ku,
                burn_in=burn_in)

# Save samples in txt file:
np.savetxt("samples_N={}_BI={}_ku={}.txt".format(N, burn_in, ku),
           my_samples, delimiter=',')
