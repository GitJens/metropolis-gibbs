# Auto-tuned Metropolis-within-Gibbs sampler

This module provides an implementation that combines the
[Metropolis-Hastings algorithm](https://en.wikipedia.org/wiki/Metropolis%E2%80%93Hastings_algorithm) and the 
[Gibbs sampler](https://en.wikipedia.org/wiki/Gibbs_sampling).
The often difficult tuning of the proposal sampler is automated.


## Background

Bayesian approaches
([[1]](https://en.wikipedia.org/wiki/Bayesian_statistics),
[[2]](https://en.wikipedia.org/wiki/Bayesian_inference))
provide an increasingly popular framework for statistical data analysis when 

1. uncertainty associated with the analysis' outcomes is of particular interest;

2. information about model parameters is available prior to the exploration of data.

Computation of the posterior distribution is commonly performed using approximation methods such as [Markov chain Monte Carlo (MCMC)](https://en.wikipedia.org/wiki/Markov_chain_Monte_Carlo).


## Advanced example

Consider the data obtained in experiments such as the ones described by
[McBride-Wright et al. (2015)](https://pubs.acs.org/doi/10.1021/je5009125)
or
[Peleties and Trusler (2011)](https://pubs.acs.org/doi/10.1021/je101256z).
Measurements of the dependent variable $`V`$ (a complex-valued voltage $`V = X + i Y \in \mathbb C`$) are linked to the independent variable $`f`$ via:
 
```math
    V = \frac {\Lambda i f} {f_0^2 - f^2 (1 + \beta) + if^2 (\beta' + 2\Delta_0)} + a + bi + c i f,
```

where $`\beta'`$ and $`\beta`$ denote the real and imaginary part of 

```math
    \beta' + i \beta = \frac{\rho}{\rho_s} \left[ i + \frac {4 i K_1(\sqrt{i \Omega})} {\sqrt{i \Omega} K_0(\sqrt{i \Omega})} \right]
```

with $`K_m(\cdot), m \in \{0,1\}`$, denoting the modified Bessel function of second kind with order $`m`$ and

```math
    \Omega = \frac {2 p f \rho R^2} {\eta}.
```
The model has 10 parameters:
$`\Lambda, f_0, \Delta_0, a,b,c, \rho, \rho_s, R, \text{ and } \eta`$. 
The main objective for the experiments is to infer about the density $`\rho`$ and the viscosity $`\eta`$.
This setup is representative for chemical engineering projects in three ways:

1. The parameters that are of particular interest ($`\rho`$ and $`\eta`$) cannot be measured directly.
2. The relationship between these parameters and the measured data ($`V, f`$) is highly non-linear.
3. Physical meanings of the parameters and past experiments provide us with some prior knowledge about the values of the parameters.

Assuming a [complex-valued Gaussian likelihood](https://en.wikipedia.org/wiki/Complex_normal_distribution) and uniform or Gaussian priors for the parameters we obtain nonlinear Bayesian regression model that can be analysed using the Metropolis-within-Gibbs sampler.



